<?php

namespace Craft;

class Vrsg_ExampleJob
{

    public $maxRetries = 1; // Will override the global setting for max job retries

    /**
     * All job classes must have a `run` method
     *
     * @param array $settings
     * @return bool
     */
    public function run($settings = []): bool
    {
        VrsgPlugin::log('Hey ho!');

        return true;
    }

}