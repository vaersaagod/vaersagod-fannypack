<?php

namespace Craft;

class Vrsg_ResaveEntriesJob {

    public function run(array $entryIds = []) : bool
    {
        $success = true;

        $entries = craft()->elements->getCriteria(ElementType::Entry, [
            'id' => $entryIds,
            'limit' => count($entryIds),
            'status' => 'any',
        ])->find();

        foreach ($entries as $entry) {
            $success = craft()->entries->saveEntry($entry);
        }

        return $success;
    }

}