<?php

namespace Craft;

use enshrined\svgSanitize\data\AllowedTags;
use enshrined\svgSanitize\data\TagInterface;

class Vrsg_CleanSvgAllowedTags extends AllowedTags implements TagInterface
{
    protected static $tags;

    public function __construct(array $allowedTags = [])
    {
        self::$tags = !empty($allowedTags) ? $allowedTags : parent::getTags();
    }

    /**
     * Returns an array of attributes
     *
     * @return array
     */
    public static function getTags(): array
    {
        return self::$tags;
    }
}
