<?php

namespace Craft;

use enshrined\svgSanitize\data\AllowedAttributes;
use enshrined\svgSanitize\data\AttributeInterface;

class Vrsg_CleanSvgAllowedAttributes extends AllowedAttributes implements AttributeInterface
{

    protected static $attrs;

    public function __construct(array $allowedAttrs = [])
    {
        self::$attrs = !empty($allowedAttrs) ? $allowedAttrs : parent::getAttributes();
    }

    /**
     * Returns an array of attributes
     *
     * @return array
     */
    public static function getAttributes(): array
    {
        return self::$attrs;
    }
}
