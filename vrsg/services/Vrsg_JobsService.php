<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg_Jobs Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class Vrsg_JobsService extends BaseApplicationComponent
{

    /**
     * @var
     */
    protected $maxRunningJobs;
    /**
     * @var
     */
    protected $maxJobDuration;
    /**
     * @var
     */
    protected $maxRetries;
    /**
     * @var
     */
    protected $types = [
        'vrsg.example' => 'Vrsg_ExampleJob',
        'vrsg.resave_entries' => 'Vrsg_ResaveEntriesJob',
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();

        Craft::import('plugins.vrsg.enums.JobStatus');

        $config = craft()->config->get('jobs', 'Vrsg') ?: [];

        $this->maxRunningJobs = $config['maxRunningJobs'] ?? 4; // Max 4 concurrent jobs
        $this->maxJobDuration = $config['maxJobDuration'] ?? 300; // Max 5 mins per job
        $this->maxRetries = $config['maxRetries'] ?? 5; // Note: can be overridden in job class

        $this->types = array_merge($this->types, $config['types'] ?: []);
    }

    /**
     * Add a job
     *
     * @param string $type
     * @param array $settings
     */
    public function add(string $type, array $settings = [])
    {
        $job = new Vrsg_JobRecord();
        $job->type = $type;
        $job->status = Vrsg_JobStatus::Pending;
        $job->settings = $settings;

        $job->save();
    }

    /**
     * Creates resave entries jobs
     *
     * @param string/array $sections handle or array of handles
     * @param bool $includeDisabled
     * @param int $chunkSize
     */
    public function resaveEntries($sections, bool $includeDisabled = false, int $chunkSize = 100)
    {

        if (!is_array($sections)) {
            $sections = [$sections];
        }

        $criteria = [
            'section' => $sections,
            'limit' => null
        ];

        if ((bool) $includeDisabled) {
            $criteria['status'] = 'any';
        }

        $entryIds = craft()->elements->getCriteria(ElementType::Entry, $criteria)->ids();
        $chunkedEntryIds = array_chunk($entryIds, (int) $chunkSize);

        foreach ($chunkedEntryIds as $entryIdsChunk) {
            $this->add('vrsg.resave_entries', $entryIdsChunk);
        }

        VrsgPlugin::log(Craft::t('Created jobs for re-saving {count} entries in sections {sections}', [
            'count' => count($entryIds),
            'sections' => implode(', ', $sections),
        ]));

        // Kick off
        craft()->vrsg->doAsyncShellCommand('vrsg_jobs', 'runPending');
    }

    /**
     * @return bool
     */
    public function hasPending()
    {
        return (bool)Vrsg_JobRecord::model()->countByAttributes([
            'status' => Vrsg_JobStatus::Pending,
        ]);
    }

    /**
     *
     */
    public function runPending()
    {
        $this->cleanHouse();
        $this->runNextPendingJob();
    }

    /**
     * @return bool
     */
    public function hasPendingJobs() : bool
    {
        return (bool)Vrsg_JobRecord::model()->countByAttributes([
            'status' => Vrsg_JobStatus::Pending,
        ]);
    }

    /**
     * @return bool
     */
    public function hasMaxRunningJobs(): bool
    {
        $count = (bool)Vrsg_JobRecord::model()->countByAttributes([
            'status' => Vrsg_JobStatus::Running,
        ]);
        return $count >= $this->maxRunningJobs;
    }

    /**
     *
     */
    protected function cleanHouse()
    {

        $now = time();

        $runningJobs = Vrsg_JobRecord::model()->findAllByAttributes([
            'status' => Vrsg_JobStatus::Running,
        ]);

        if ($runningJobs && !empty($runningJobs)) {
            foreach ($runningJobs as $job) {

                if (($now - $job->dateUpdated->getTimestamp()) >= $this->maxJobDuration) {

                    // This job has been running for too long, and is probably stalled
                    // Set it to pending or failed
                    if ($job->tries < $this->maxRetries) {
                        $job->error = null;
                        $job->status = Vrsg_JobStatus::Pending;
                    } else {
                        $job->error = Craft::t('Stalled job; marked as failed after {tries} tries', [
                            'tries' => $job->tries,
                        ]);
                        $job->status = Vrsg_JobStatus::Failed;
                    }

                    $job->update();
                }
            }
        }

        // TODO:
        // Very old, completed jobs should probably/maybe be deleted to save disk space
    }

    /**
     * @return static
     */
    protected function getNextPendingJob()
    {
        return Vrsg_JobRecord::model()->findByAttributes([
            'status' => Vrsg_JobStatus::Pending
        ], [
            'order' => 'id ASC',
        ]);
    }

    /**
     *
     */
    protected function runNextPendingJob()
    {

        $job = $this->getNextPendingJob();

        if (!$job || $this->hasMaxRunningJobs()) {
            return;
        }

        $job->error = null;
        $job->status = Vrsg_JobStatus::Running;
        $job->tries = ($job->tries ?? 0) + 1;
        $job->update();

        $success = false;
        $error = null;
        $instance = null;

        try {

            $className = $this->types[$job->type] ?? null;

            if (!$className) {
                throw new Exception(Craft::t('Job type "{type}" missing from config', [
                    'type' => $job->type,
                ]));
            }

            $pluginHandle = strtolower(explode('_', $className)[0]);
            $classPath = "plugins.{$pluginHandle}.jobs.{$className}";
            $namespacedClassName = "Craft\\{$className}";

            Craft::import($classPath);

            if (!class_exists($namespacedClassName)) {
                throw new Exception(Craft::t('Class "{namespacedClassName}" not found (path is {classPath})', [
                    'namespacedClassName' => $namespacedClassName,
                    'classPath' => $classPath,
                ]));
            }

            $instance = new $namespacedClassName();

            if (!method_exists($instance, 'run')) {
                throw new Exception(Craft::t('Class "{namespacedClassName}" doesn\'t have a `run` method', [
                    'namespacedClassName' => $namespacedClassName,
                ]));
            }

            $success = $instance->run($job->settings);

        } catch (Exception $e) {

            $error = $e->getMessage();

        }

        if ($success === false || $error) {

            $maxJobRetries = $instance && isset($instance->maxRetries) ? $instance->maxRetries : $this->maxRetries;

            if ($job->tries < $maxJobRetries) {
                $job->status = Vrsg_JobStatus::Pending;
            } else {
                $job->status = Vrsg_JobStatus::Failed;
            }

            $job->error = $error ?? Craft::t('Job failed for some unknown reason');

        } else {

            $job->status = Vrsg_JobStatus::Complete;
        }

        $job->update();

    }

}