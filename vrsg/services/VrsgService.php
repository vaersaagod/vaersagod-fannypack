<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class VrsgService extends BaseApplicationComponent
{

    /**
     * Wrapper for shell console commands
     *
     * @param string $command
     * @param string|null $action
     * @param array|null $params
     * @param bool $async
     */
    public function doShellCommand(string $command, string $action = null, array $params = null, $async = false)
    {
        $args = null;
        if ($params) {
            $args = [];
            foreach ($params as $key => $value) {
                $args[] = "--$key=$value";
            }
            $args = implode(' ', $args);
        }

        $cmd = 'php ' . dirname(CRAFT_BASE_PATH) . '/yiic.php ' . $command . ($action ? ' ' . $action : '') . ($args ? ' ' . $args : '');

        if ($async) {
            shell_exec($cmd . '  > /dev/null 2>/dev/null &');
        } else {
            system($cmd);
        }

    }

    /**
     * Wrapper for shell console commands
     *
     * @param string $command
     * @param string|null $action
     * @param array|null $params
     */
    public function doAsyncShellCommand(string $command, string $action = null, array $params = null)
    {
        $this->doShellCommand($command, $action, $params, true);
    }

}