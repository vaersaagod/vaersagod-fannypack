<?php

return [

    /*
     * Jobs, jobs, jobs!
     *
     */
    'jobs' => [
        'maxRunningJobs' => 1,
        'maxRetries' => 5,
        'types' => [
            'somehandle' => 'FooPluginHandle_SomeJob',
        ],
    ],

    /*
     * Rollbar
     *
     */
    'rollbarServerToken' => null,
];