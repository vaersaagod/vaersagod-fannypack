<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * VrsgHelper
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class VrsgHelper
{

    /**
     * Fixes slashes in path
     *
     * @param            $str
     * @param bool|false $removeInitial
     * @param bool|false $removeTrailing
     *
     * @return mixed|string
     */
    public static function fixSlashes($str, $removeInitial = false, $removeTrailing = false)
    {
        $str = preg_replace('/([^:])(\/{2,})/', '$1/', $str);
        if ($removeInitial) {
            $str = ltrim($str, '/');
        }
        if ($removeTrailing) {
            $str = rtrim($str, '/');
        }
        return $str;
    }


    /**
     * Check if a string is valid JSON
     *
     * @param string $string
     * @return bool
     */
    public static function isJson(string $string) : bool
    {
        json_decode($string);
        return json_last_error() == JSON_ERROR_NONE;
    }

    /**
     * Download a remote file
     *
     * @param $fileUrl
     * @param $filePath
     *
     * @return bool
     */
    public static function downloadFile(string $fileUrl, string $filePath)
    {

        $httpStatus = null;
        $errorMessage = null;

        IOHelper::ensureFolderExists($filePath);

        if (function_exists('curl_init')) {

            VrsgPlugin::log(Craft::t('Downloading {fileUrl} using cURL', [
                'fileUrl' => $fileUrl,
            ]));

            $ch = curl_init($fileUrl);
            $fp = fopen($filePath, "wb");

            $options = [
                CURLOPT_FILE => $fp,
                CURLOPT_HEADER => 0,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_TIMEOUT => 60,
            ];

            curl_setopt_array($ch, $options);
            curl_exec($ch);

            if (curl_errno($ch)) {
                $errorMessage = curl_error($ch);
            }

            $httpStatus = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));

            curl_close($ch);
            fclose($fp);
        } elseif (ini_get('allow_url_fopen')) {

            VrsgPlugin::log(Craft::t('Downloading {fileUrl} using fopen', [
                'fileUrl' => $fileUrl,
            ]));

            file_put_contents($filePath, file_get_contents($fileUrl));
            $httpStatus = $http_response_header[0] ?? null;
        } else {

            throw new Exception(Craft::t('Looks like allow_url_fopen is off and cURL is not enabled. To download external files, one of these methods has to be enabled.'));
        }

        if ($errorMessage) {
            throw new Exception(Craft::t('An error “{errorMessage}” occurred while attempting to download “{fileUrl}”', [
                'fileUrl' => $fileUrl,
                'errorMessage' => $errorMessage
            ]));
        }

        if ($httpStatus !== 200) {
            throw new Exception(Craft::t('HTTP status “{httpStatus}” encountered while attempting to download “{fileUrl}”', [
                'fileUrl' => $fileUrl,
                'httpStatus' => $httpStatus
            ]));
        }

        return true;
    }

}