<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Command
 *
 * --snip--
 * Craft is built on the Yii framework and includes a command runner, yiic in ./craft/app/etc/console/yiic
 *
 * Action methods are mapped to command-line commands, and begin with the prefix “action”, followed by
 * a description of what the method does (for example, actionPrint().  The actionIndex() method is what
 * is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft/app/etc/console/yiic vrsg
 *
 * The actionPrint() method above would be invoked via:
 *
 * ./craft/app/etc/console/yiic vrsg print
 *
 * http://spin.atomicobject.com/2015/06/16/craft-console-plugin/
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class VrsgCommand extends BaseCommand
{
    /*
     * Clears all template caches
     */
    public function actionClearTemplateCaches()
    {
        craft()->templateCache->deleteAllCaches();
        craft()->end('OK');
    }

    /*
     * Runs pending tasks. If you have the `runTasksAutomatically` config setting disabled, this command can be run as a cron job every minute or so
     */
    public function actionRunPendingTasks()
    {
        craft()->tasks->runPendingTasks();
        craft()->end('OK');
    }

    /*
     * Craft can sometimes build up a healthy Asset cache as time goes on
     * This command will clear out original (i.e. full-size) images from the Assets cache (won't touch thumnbails) and can be run as a cron job every 12hrs or so
     */
    public function actionClearSourcesFromAssetCaches()
    {
        $assetCachePath = rtrim(CRAFT_STORAGE_PATH, '/') . '/runtime/assets/sources';
        IOHelper::clearFolder($assetCachePath, true);
        craft()->end('OK');
    }

    /**
     * @param string $sections Comma-separeted list of section handles (e.g. "news,homepage")
     * @param null $includeDisabled
     * @param int $chunkSize
     */
    public function actionResaveEntries(string $sections, $includeDisabled = false, $chunkSize = 100)
    {
        craft()->vrsg_jobs->resaveEntries(explode(',', $sections), (bool)$includeDisabled, (int)$chunkSize);
        craft()->end('OK');
    }
}