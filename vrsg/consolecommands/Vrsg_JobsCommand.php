<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Command
 *
 * --snip--
 * Craft is built on the Yii framework and includes a command runner, yiic in ./craft/app/etc/console/yiic
 *
 * Action methods are mapped to command-line commands, and begin with the prefix “action”, followed by
 * a description of what the method does (for example, actionPrint().  The actionIndex() method is what
 * is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft/app/etc/console/yiic vrsg
 *
 * The actionPrint() method above would be invoked via:
 *
 * ./craft/app/etc/console/yiic vrsg print
 *
 * http://spin.atomicobject.com/2015/06/16/craft-console-plugin/
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class Vrsg_JobsCommand extends BaseCommand
{

    /**
     *  Run pending jobs
     */
    public function actionRunPending()
    {

        VrsgPlugin::log(Craft::t('Running pending jobs...'));
        craft()->vrsg_jobs->runPending();

        // Play it again, Sam
        if (!craft()->vrsg_jobs->hasMaxRunningJobs() && craft()->vrsg_jobs->hasPendingJobs()) {
            craft()->vrsg->doAsyncShellCommand('vrsg_jobs', 'runPending');
        }

        craft()->end('OK');

    }

}