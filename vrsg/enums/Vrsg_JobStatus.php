<?php

namespace Craft;

abstract class Vrsg_JobStatus extends BaseEnum
{
    const Pending = 'pending';
    const Failed = 'failed';
    const Complete = 'complete';
    const Running = 'running';
}