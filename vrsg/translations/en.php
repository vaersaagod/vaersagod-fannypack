<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Translation
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
