<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Variable
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

class VrsgVariable
{

    /**
     * Returns URL to a Static Google Maps map
     * Usage example:
     *
     * {% set staticMapUrl = craft.vrsg.getStaticGoogleMapsUrl({ center: 'Hønefoss,Norway' }) %}
     *
     *
     * @param array $params
     * @param string $apiKey
     * @return string
     * @throws Exception
     */
    public function getStaticGoogleMapsMapUrl(array $params = [], string $apiKey = '')
    {
        $apiKey = $apiKey ?: craft()->config->get('googleApiKey', 'vrsg') ?: craft()->config->get('googleApiKey');

        if (!$apiKey) {
            throw new Exception(Craft::t('Missing API key for Google Maps Static API'));
        }

        $serviceUrl = 'https://maps.googleapis.com/maps/api/staticmap';

        $params = array_merge([
            'center' => 'Oslo,Norway',
            'zoom' => 8,
            'size' => '2000x2000',
            'scale' => 2,
        ], $params);

        $url = $serviceUrl . '?' . http_build_query($params) . '&key=' . $apiKey;

        return $url;
    }

    /**
     * Returns array w/ "width" and "height" for an SVG Asset
     * The width and height is pulled from the viewbox, or from the width and height attributes if no viewbox attribute is present
     * Pass `false` as the second parameter to ignore the viewbox and pull directly from "width" and "height" attributes
     *
     * @param AssetFileModel $svg
     * @param bool $useViewBox
     * @return array
     */
    public function getSvgDimensions(AssetFileModel $svg, bool $useViewBox = true)
    {

        if (strtolower($svg->extension) !== 'svg') {
            throw new Exception(Craft::t('The file {filename} is not an SVG', [
                'filename' => $svg->filename,
            ]));
        }

        $source = $svg->getSource();
        if ($source->type != 'Local') {
            throw new Exception(Craft::t('Paths not available for non-local asset sources'));
        }

        $sourcePath = $source->settings['path'];
        $folderPath = $svg->getFolder()->path;
        $assetFilePath = craft()->config->parseEnvironmentString($sourcePath.$folderPath.$svg->filename);

        if (!IOHelper::fileExists($assetFilePath)) {
            throw new Exception(Craft::t('File {filepath} not found', [
                'filepath' => $assetFilePath,
            ]));
        }

        $svgFile = IOHelper::getFileContents($assetFilePath);
        $svgXml = simplexml_load_string($svgFile, 'SimpleXMLElement', LIBXML_NOCDATA);
        $svgAttributes = $svgXml->attributes();

        $width = null;
        $height = null;

        if ($useViewBox) {
            $viewBox = (string) ($svgAttributes['viewBox'] ?? $svgAttributes['viewbox'] ?? null);
            if ($viewBox) {
                $temp = explode(' ', $viewBox);
                $width = (int) ($temp[2] ?? null);
                $height = (int) ($temp[3] ?? null);
            }
        }

        $width = $width ? $width : (int) ($svgAttributes['width'] ?? null);
        $height = $height ? $height : (int) ($svgAttributes['height'] ?? null);

        return [
            'width' => $width,
            'height' => $height,
        ];

    }

}
