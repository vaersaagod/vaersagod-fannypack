<?php

/*
 * yiic bootstrapper
 *
 * Boots yiic w/ proper constants set.
 * Usage:
 *
 * $ php /path/to/yiic.php command action --arg=value
 *
 */

if (!defined('CRAFT_ENVIRONMENT')) {

    require_once(__DIR__ . '/vendor/autoload.php');

    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();
    $dotenv->required(['CRAFT_ENVIRONMENT', 'DB_SERVER', 'DB_DATABASE', 'DB_USER', 'DB_PASSWORD']);

    define('CRAFT_CONFIG_PATH', __DIR__ . '/config/');
    define('CRAFT_PLUGINS_PATH', __DIR__ . '/vendor/');
    define('CRAFT_STORAGE_PATH', __DIR__ . '/storage/');
    define('CRAFT_TEMPLATES_PATH', __DIR__ . '/templates/');
    define('CRAFT_TRANSLATIONS_PATH', __DIR__ . '/translations/');
    define('CRAFT_ENVIRONMENT', getenv('CRAFT_ENVIRONMENT'));
}

require(__DIR__ . '/craft/app/etc/console/yiic.php');