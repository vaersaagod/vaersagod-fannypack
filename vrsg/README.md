# Vrsg plugin for Craft CMS

Værsågod Fanny Pack

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Vrsg, follow these steps:

1. Download & unzip the file and place the `vrsg` directory into your `craft/plugins` directory
2.  -OR- do a `git clone https://github.com/mmikkel/vrsg.git` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require mmikkel/vrsg`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `vrsg` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Vrsg works on Craft 2.4.x and Craft 2.5.x.

## Vrsg Overview

-Insert text here-

## Configuring Vrsg

-Insert text here-

## Using Vrsg

-Insert text here-

## Vrsg Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Værsågod](https://vaersaagod.no)
