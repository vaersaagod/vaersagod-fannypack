<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Værsågod Fanny Pack
 *
 * --snip--
 * Craft plugins are very much like little applications in and of themselves. We’ve made it as simple as we can,
 * but the training wheels are off. A little prior knowledge is going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL, as well as some semi-
 * advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;
use \Rollbar\Rollbar;

class VrsgPlugin extends BasePlugin
{

    /**
     * @var
     */
    private $version = '1.4.6';
    private $schemaVersion = '1.0.0';
    private $name = 'Vrsg';
    private $description = 'Værsågod Fannypack';
    private $documentationUrl = 'https://bitbucket.org/vaersaagod/vaersagod-fannypack/raw/master/README.md';

    /**
     * @var
     */
    protected static $rollbarEnabled;

    /**
     * Called after the plugin class is instantiated; do any one-time initialization here such as hooks and events:
     *
     * craft()->on('entries.saveEntry', function(Event $event) {
     *    // ...
     * });
     *
     * or loading any third party Composer packages via:
     *
     * require_once __DIR__ . '/vendor/autoload.php';
     *
     * @return mixed
     */
    public function init()
    {
        parent::init();
        require_once __DIR__.'/vendor/autoload.php';

        // Import helper class
        Craft::import('plugins.vrsg.helpers.VrsgHelper');

        $this->maybeInitRollbar();
    }

    /**
     * @return bool
     */
    public function maybeInitRollbar()
    {

        $accessToken = craft()->config->get('rollbarServerToken', 'vrsg');

        if (!$accessToken) {
            return false;
        }

        // Init Rollbar
        Rollbar::init([
            'access_token' => $accessToken,
            'environment' => CRAFT_ENVIRONMENT,
            'root' => craft()->config->get('environmentVariables')['publicRootPath'] ?? null,
        ], false, false);

        // Log Craft Exceptions to Rollbar
        craft()->onException = function($event) {
            $statusCode = $event->exception->statusCode ?? null;
            if ($statusCode !== 404) {
                Rollbar::log(LogLevel::Error, $event->exception);
            }
        };

        // Log Craft Errors to Rollbar
        craft()->onError = function($event) {
            Rollbar::log(LogLevel::Error, $event->message);
        };

        self::$rollbarEnabled = true;
    }

    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
         return Craft::t($this->name);
    }

    /**
     * Plugins can have descriptions of themselves displayed on the Plugins page by adding a getDescription() method
     * on the primary plugin class:
     *
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t($this->description);
    }

    /**
     * Plugins can have links to their documentation on the Plugins page by adding a getDocumentationUrl() method on
     * the primary plugin class:
     *
     * @return string
     */
    public function getDocumentationUrl()
    {
        return $this->documentationUrl;
    }

    /**
     * Plugins can now take part in Craft’s update notifications, and display release notes on the Updates page, by
     * providing a JSON feed that describes new releases, and adding a getReleaseFeedUrl() method on the primary
     * plugin class.
     *
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return 'https://raw.githubusercontent.com/mmikkel/vrsg/master/releases.json';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a plugin’s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return $this->schemaVersion;
    }

    /**
     * Returns the developer’s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Værsågod';
    }

    /**
     * Returns the developer’s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'https://vaersaagod.no';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     * Add any Twig extensions.
     *
     * @return mixed
     */
    public function addTwigExtension()
    {
        Craft::import('plugins.vrsg.twigextensions.VrsgTwigExtension');

        return new VrsgTwigExtension();
    }

    /**
     * @return array
     */
    public function registerSiteRoutes()
    {
        $routes = [];

        // Add /ping route if there's no "ping.twig" template in the site templates folder
        $siteTemplatesPath = rtrim(craft()->path->getSiteTemplatesPath(), '/') . '/';
        $addPingRoute = !IOHelper::fileExists($siteTemplatesPath . 'ping.twig');

        if ($addPingRoute) {
            $routes['ping'] = ['action' => 'vrsg/ping'];
        }

        return $routes;
    }

    /**
     * @param string     $msg
     * @param string     $loglevel
     * @param bool       $force
     * @param array|null $args
     */
    public static function log($msg, $loglevel = LogLevel::Info, $force = CRAFT_ENVIRONMENT === 'local', array $args = null)
    {

        if (!$force) {
            $force = craft()->config->get('devMode');
        }

        parent::log($msg, $loglevel, $force);

        if (class_exists('Rollbar\Rollbar') && self::$rollbarEnabled && ($force || $loglevel !== LogLevel::Info)) {
            Rollbar::log($loglevel, $msg, $args);
        }
    }

    /**
     * Called right before your plugin’s row gets stored in the plugins database table, and tables have been created
     * for it based on its records.
     */
    public function onBeforeInstall()
    {
    }

    /**
     * Called right after your plugin’s row has been stored in the plugins database table, and tables have been
     * created for it based on its records.
     */
    public function onAfterInstall()
    {
    }

    /**
     * Called right before your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onBeforeUninstall()
    {
    }

    /**
     * Called right after your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onAfterUninstall()
    {
    }
}
