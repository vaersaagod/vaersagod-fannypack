<?php
/**
 * Vrsg plugin for Craft CMS
 *
 * Vrsg Twig Extension
 *
 * --snip--
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and
 * functions. You can even extend the parser itself with node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 * --snip--
 *
 * @author    Værsågod
 * @copyright Copyright (c) 2017 Værsågod
 * @link      https://vaersaagod.no
 * @package   Vrsg
 * @since     1.0.0
 */

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;
use enshrined\svgSanitize\Sanitizer;

class VrsgTwigExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'Vrsg';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'cleanWhitespace' => new \Twig_Filter_Method($this, 'cleanWhitespaceFilter'),
            'removeWhitespace' => new \Twig_Filter_Method($this, 'removeWhitespaceFilter'),
            'jsonDecode' => new \Twig_Filter_Method($this, 'jsonDecodeFilter'),
            'ksort' => new \Twig_Filter_Method($this, 'ksortFilter'),
            'unique' => new \Twig_Filter_Method($this, 'uniqueFilter'),
            'truncateHtml' => new \Twig_Filter_Method($this, 'truncateHtmlFilter'),
            'toFloat' => new \Twig_Filter_Method($this, 'toFloatFilter'),
            'toInt' => new \Twig_Filter_Method($this, 'toIntFilter'),
            'md5' => new \Twig_Filter_Method($this, 'md5Filter'),
            'camelToSnake' => new \Twig_Filter_Method($this, 'camelToSnakeFilter'),
            'camelToKebab' => new \Twig_Filter_Method($this, 'camelToKebabFilter'),
            'widont' => new \Twig_Filter_Method($this, 'widontFilter'),
            'cleanSvg' => new \Twig_Filter_Method($this, 'cleanSvgFilter'),
        );
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'cos' => new \Twig_Function_Method($this, 'cosFunction'),
            'sin' => new \Twig_Function_Method($this, 'sinFunction'),
            'pi' => new \Twig_Function_Method($this, 'piFunction'),
        );
    }

    /**
     * @param $html
     * @return Twig_Markup
     */
    public function cleanWhitespaceFilter($html, $getRaw = true)
    {
        // replace UTF-8 non breaking space with html entity
        $html = preg_replace('~\x{00a0}~siu', '&nbsp;', $html);

        // Remove duplicates
        $html = preg_replace("#(<br\s*/?>\s*)+#", "<br />", $html);

        // Remove orphan
        $html = str_replace("<p><br /></p>", "", $html);

        // Remove leading
        $html = preg_replace("#<p>(<br\s*/?>\s*)+#", "<p>", $html);

        // Remove trailing
        $html = preg_replace("#(<br\s*/?>\s*)+</p>#", "</p>", $html);

        // Remove paragraphs with whitespace and non-breaking space
        $html = preg_replace("#<p>(\s*(&nbsp;)*)*<\/p>#", "", $html);

        return $getRaw ? TemplateHelper::getRaw($html) : $html;
    }

    /**
     * @param $text
     * @return Twig_Markup
     */
    public function removeWhitespaceFilter($text, $getRaw = true)
    {
        $text = preg_replace('/\s+/', '', $text);
        return $getRaw ? TemplateHelper::getRaw($text) : $text;
    }

    /**
     * @param array $array
     * @param int $sortFlag
     * @return array
     */
    public function ksortFilter(array $array, $sortFlag = SORT_REGULAR)
    {
        ksort($array, $sortFlag);
        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    public function uniqueFilter(array $array)
    {
        return array_unique($array);
    }

    /**
     * @param $val
     * @return mixed
     */
    public function jsonDecodeFilter($val, $assoc = false)
    {
        return json_decode($val, $assoc);
    }

    /**
     *
     * Stolen from CakePHP, see https://stackoverflow.com/a/2398759/4663184 and
     *
     * @param $text
     * @param int $length
     * @param string $ending
     * @param bool $exact
     * @param bool $considerHtml
     * @return string
     */
    public function truncateHtmlFilter($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false)
    {
        if (is_array($ending)) {
            extract($ending);
        }
        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0, $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }

        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }

        return TemplateHelper::getRaw($truncate);
    }

    /**
     * @param $input
     * @return float
     */
    public function toFloatFilter($input)
    {
        return (float)$this->removeWhitespaceFilter(strip_tags($input), false);
    }

    /**
     * @param $input
     * @return int
     */
    public function toIntFilter($input)
    {
        return (int)$this->removeWhitespaceFilter(strip_tags($input), false);
    }

    /**
     * @param $input
     * @return string
     */
    public function md5Filter($input)
    {
        return md5($input);
    }

    /**
     * @param $input
     * @return string
     */
    public function camelToSnakeFilter($input)
    {
        return $this->convertCamelCase($input, '_');
    }

    /**
     * @param $input
     * @return string
     */
    public function camelToKebabFilter($input)
    {
        return $this->convertCamelCase($input, '-');
    }

    /**
     * @param $input
     * @return \Twig_Markup
     */
    public function widontFilter($input)
    {
        $expression = "/([^\s])\s+(((<(a|span|i|b|em|strong|acronym|caps|sub|sup|abbr|big|small|code|cite|tt)[^>]*>)*\s*[^\s<>]+)(<\/(a|span|i|b|em|strong|acronym|caps|sub|sup|abbr|big|small|code|cite|tt)>)*[^\s<>]*\s*(<\/(p|h[1-6]|li)>|$))/i";
        return TemplateHelper::getRaw(preg_replace($expression, '$1&nbsp;$2', $input));
    }

    /**
     * @param string $input
     * @param array $opts
     * @return string
     */
    public function cleanSvgFilter(string $input, array $opts = []) : string
    {

        Craft::import('plugins.vrsg.library.Vrsg_CleanSvgAllowedTags');
        Craft::import('plugins.vrsg.library.Vrsg_CleanSvgAllowedAttributes');

        $sanitizer = new Sanitizer();

        $opts = array_merge([
            'allowedAttrs' => [],
            'allowedTags' => [],
            'removeRemoteReferences' => true,
            'minify' => true,
        ], $opts);

        $sanitizer->setAllowedAttrs(new Vrsg_CleanSvgAllowedAttributes($opts['allowedAttrs']));
        $sanitizer->setAllowedTags(new Vrsg_CleanSvgAllowedTags($opts['allowedTags']));
        $sanitizer->removeRemoteReferences($opts['removeRemoteReferences']);
        $sanitizer->minify($opts['minify']);

        $result = $sanitizer->sanitize($input);

        if ($result) {
            return TemplateHelper::getRaw($result);
        }

        return '';
    }

    /**
     * @param $value
     * @return float
     */
    public function cosFunction($value)
    {
        return cos($value);
    }

    /**
     * @param $value
     * @return float
     */
    public function sinFunction($value)
    {
        return sin($value);
    }

    /**
     * @return float
     */
    public function piFunction()
    {
        return pi();
    }

    /*
     * Private methods
     *
     */
    /**
     * @param $input
     * @param $delimiter
     * @return string
     */
    private function convertCamelCase($input, $delimiter)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('-', $ret);
    }

}
